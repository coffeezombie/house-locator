Developer Town - Shack Tracker
===============================
A touch screen application, created for Developer Town, to aid visitors in finding employees' offices. 

Getting Started
--------------
This repository holds both the backend (Node.js) and frontend (AngularJS) of the application.

### Frontend
The frontend uses Gulp to compile SASS into CSS. If you are wanting to make edits to the css:

In the `/frontend` folder, `sudo npm install` to install Gulp packages. 
Then, run `gulp` in the `/frontend` folder to watch for changes to files in `/frontend/assets/scss`.

You can change the API location in `/frontend/config/config.js` if you will be running the backend from a different location or port. 


### Backend
Install [ImageMagick](https://github.com/hacksparrow/node-easyimage).

Run `sudo npm install` in the `/backend` folder to install dependencies. 

Copy the `/backend/config/parameters.js.example` and rename it to `/backend/config/parameters.js`. 
Fill out the parameters for the database (not needed if using SQLite) and Twilio (you can sign up for a free account).

Run the backend with `node main.js`.