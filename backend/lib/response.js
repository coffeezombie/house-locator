exports.success = function(response, data, code) {
    var responseData = {};
    responseData = data;

    return response.json(code || 200, responseData);
}

exports.fail = function(response, data, code) {
    var responseData = {};
    responseData.status = 'fail';
    responseData.data = data;

    return response.json(code || 400, responseData);
}

exports.failWithMessage = function(response, message, code) {
    var responseData = {};
    responseData.status = 'fail';
    responseData.message = message;

    return response.json(code || 400, responseData);
}

exports.error = function(response, message, error, code) {
    var responseData = {};
    responseData.status = 'error';
    responseData.message = message;
    responseData.error = error;

    return response.json(code || 500, responseData);
}

exports.missingParamsData = function(missingParams) {
    var data = {};
    for(var i in missingParams) {
        var missingParam = missingParams[i];
        data[missingParam] = 'Missing required parameter: ' + missingParam;
    }
    return data;
}