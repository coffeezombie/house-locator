var fs = require('fs');
var jwt = require('jwt-simple');
var config = require(process.cwd() + '/config/config');
var Response = require(process.cwd() + '/lib/response');

/* =========================================== */

exports.requireAppFiles = function(folderName, fileSuffix) {
    var path = process.cwd() + '/app/' + folderName;
    var files = {};

    fs.readdirSync(path).forEach(function (file) {
        if (file.indexOf('.js') != -1) {
            files[file.split(fileSuffix + '.')[0]] = require(path + '/' + file);
        }
    });

    return files;
}

exports.validateParameters = function(requiredParams, request) {
    var requestParams = request.params;
    var requestKeys = Object.keys(requestParams);

    return {
        requestParams:  requestKeys,
        requiredParams: requiredParams,
        missingParams:  exports.arrayDiff(requiredParams, requestKeys)
    };
}

exports.validationResponse = function(requiredParams, request, response) {
    var validation = exports.validateParameters(requiredParams, request);

    if(validation.missingParams.length > 0) {
        return Response.fail(response, Response.missingParamsData(validation.missingParams), 422);
    }

    return false;
}

exports.params = function(params) {
    if(typeof params === 'undefined') {
        return null;
    }

    if(typeof params === 'object') { // Object
        var data = {};
        for(var key in params) {
            var param = params[key];
            if(typeof param === 'undefined') {
                data[key] = null;
            }

            if (typeof param === 'string') {
                data[key] = param;
            }
            data[key] = JSON.parse(param);
        }
    } else { // Single
        if (typeof params === 'string') {
            return params;
        }
        return JSON.parse(params);
    }
}

exports.generateToken = function(user, days) {
    var expires = new Date()
    expires.setDate(expires.getDate() + days);

    var token = jwt.encode({
        userId: user.id,
        expires: expires
    }, config.auth.secret);

    return token;
}

exports.arrayDiff = function(a, b) {
    return a.filter(function(i) {return b.indexOf(i) < 0;});
};

exports.random = function(min, max) {
    return Math.floor((Math.random() * max) + min);
};

exports.sanitizePhone = function(phone)
{
    // Remove non numeric
    phone = phone.replace(/\D/g, "");

    if(phone.length == 11) { // 1 555 555 5555
        return "+" + phone;
    } else if(phone.length == 10) { // 555 555 5555
        return "+1" + phone;
    }

    // No area code or wrong number of digits
    return false;
};