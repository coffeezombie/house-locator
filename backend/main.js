var Sequelize = require('sequelize');
var Restify = require('restify');
var Twilio = require('twilio');
var fs = require('fs');
var socketio = require('socket.io');

var config = require('./config/config');
var Controllers = require('./controllers');
var Models = require('./models');
var util = require('./lib/util');

/* =========================================== */

var googleConfig = {
    clientID: '5695497471-ngfce2tsg8dknhkeg2n34l1071s362cr.apps.googleusercontent.com',
    clientSecret: 'PWqJ7l-cZPB51AllzCmP1Fus',
    calendarId: 'developertown.com_2d3435393833343638323537@resource.calendar.google.com',
    redirectURL: 'http://localhost:3001/auth' //HUH?
};

var models = {};

// Set up server
var server = Restify.createServer({name: config.app.name});
server.use( Restify.fullResponse() ).use( Restify.bodyParser() ).use( Restify.queryParser()) ;

server.use( Restify.CORS() );
server.pre( Restify.CORS() );

//server.get('/\.*/', function(req, res, next) {
//   res.setHeader('Access-Control-Allow-Credentials', true);
//    return next;
//});

//server.opts('/\.*/', function(req, res, next) {
//    res.setHeader('Access-Control-Allow-Origin', '*');
//    res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
//    return next();
//}, function(req, res, next) {
//    res.send(200);
//    return next();
//});
//
//server.post('/\.*/', function(req, res, next) {
//    res.setHeader('Access-Control-Allow-Origin', '*');
//    res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
//    return next();
//});


// Set up socket.io
var io = socketio.listen(server.server);

io.on('connection', function(socket){
    socket.on('update', function(msg){
        //io.emit('update', msg);
        socket.broadcast.emit('update', msg);
    });
});

// Set up Twilio
var twilio = new Twilio.RestClient(config.twilio.sid, config.twilio.authToken);

// Set up ORM and models
var sequelize = new Sequelize(config.database.name, config.database.username, config.database.password, config.database);

sequelize.authenticate().complete(function(err) {
    if (err) {
        console.log('Unable to connect to the database:', err);
        return;
    }

    models = Models(sequelize);

    sequelize.sync({ force: false }).complete(function(err) {
        if (err) {
            console.log('An error occurred while creating the table:', err);
            return;
        }

        init();
    });

});

// Initialize
var init = function()
{
    // Set up controllers
    var controllers = Controllers(server, models, io, twilio);

    // Start server
    server.listen(config.app.port, function (error) {
        if (error) {
            return console.error(error);
        }

        console.log('%s listening at %s', server.name, server.url);
    });

    // Production error handling
    if (process.env.environment == 'production') {
        process.on('uncaughtException', function (error) {
            console.error(JSON.parse(JSON.stringify(error, ['stack', 'message', 'inner'], 2)));
        });
    }
}