module.exports = function(sequelize) {
    var models = {};

    models.employee = require(process.cwd() + "/models/employeeModel")(sequelize);
    models.house = require(process.cwd() + "/models/houseModel")(sequelize);
    models.user = require(process.cwd() + "/models/userModel")(sequelize);
    models.session = require(process.cwd() + "/models/sessionModel")(sequelize);

    models.employee.hasOne(models.house);
    models.house.belongsTo(models.employee);

    return models;
}