var Response = require(process.cwd() + '/lib/response');
var Util = require(process.cwd() + '/lib/util');
var fs = require('fs');
var bcrypt = require('bcrypt-nodejs');

module.exports = function(server, authenticator, io, models) {
    var resource = '/users';

    /*
     * Get users
     */
    server.get(resource + "/", authenticator.authenticate, function (request, response, next) {
        models.user.findAll().complete(function(err, users) {
            if (err) {
                console.log("\nError: " + err);
                return Response.error(response, "There was a problem getting users.");
            }

            for(var i in users) {
                var user = users[i];
                // Remove password
                user.password = undefined;
                delete user.password;
            }

            return Response.success(response, users);
        });
    });

    /*
     * Get single user
     */
    server.get(resource + "/:id", authenticator.authenticate, function (request, response, next) {
        if(Util.validationResponse(['id'], request, response)) {
            return;
        }

        var params = {
            id:  Util.params(request.params.id)
        }

        models.user.find({ where: {id: params.id} }).complete(function(err, user) {
            if (err) {
                console.log("\nError: " + err);
                return Response.error(response, "There was a problem getting user.");
            }

            // Remove password
            user.password = undefined;
            delete user.password;

            return Response.success(response, user);
        });
    });

    /*
     * Create user
     */
    server.post(resource + "/", authenticator.authenticate, function (request, response, next) {
        if(Util.validationResponse(['username', 'password'], request, response)) {
            return;
        }

        var params = {
            username:   Util.params(request.params.username),
            password:   Util.params(request.params.password)
        }

        if(validateUserParams(params, response)) {
            return;
        }

        params.password = bcrypt.hashSync(params.password);

        models.user.create(params).complete(function(err, user) {
            if (err) {
                console.log("\nError: " + err);
                return Response.error(response, "There was a problem creating user: " + err);
            }

            // Remove password
            user.password = undefined;
            delete user.password;

            // io.emit('update', true);

            return Response.success(response, user);
        });
    });

    /*
     * Update user
     */
    server.put(resource + "/:id", authenticator.authenticate, function (request, response, next) {
        if(Util.validationResponse(['id'], request, response)) {
            return;
        }

        var params = {
            id:         Util.params(request.params.id),
            username:   Util.params(request.params.username)
        }

        if(request.params.password) {
            params.password = Util.params(request.params.password);
            params.password = bcrypt.hashSync(params.password);
        }

        if(validateUserParams(params, response)) {
            return;
        }

        models.user.find({ where: {id: params.id}}).complete(function(err, user) {
            if (err) {
                console.log("\nError: " + err);
                return Response.error(response, "There was a problem getting user.");
            }
            if(user === null) {
                return Response.error(response, "Couldn't find user.");
            }

            user.update(params, {where: {id: params.id}}).complete(function(err) {
                if(err) {
                    console.log("\nError: " + err);
                    return Response.error(response, "There was a problem saving user.");
                }

                // Remove password
                user.password = undefined;
                delete user.password;

                // io.emit('update', true);

                return Response.success(response, user);
            });
        });
    });


    /*
     * Delete user
     */
    server.del(resource + "/:id", authenticator.authenticate, function (request, response, next)
    {
        if(Util.validationResponse(['id'], request, response)) {
            return;
        }

        var params = {
            id:  Util.params(request.params.id)
        }

        models.user.find({ where: {id: params.id}}).complete(function(err, user) {
            if (err) {
                console.log("\nError: " + err);
                return Response.error(response, "There was a problem getting user.");
            }

            user.destroy().complete(function(err) {
                if(err) {
                    console.log("\nError: " + err);
                    return Response.error(response, "There was a problem deleting user.");
                }

                // io.emit('update', true);

                return Response.success(response, true);
            });
        });

    });

}

function validateUserParams(params, response)
{
    // Very simple and crude and not exhaustive...

    if(params.username === "") {
        return Response.failWithMessage(response, "User must have a username.");
    }

    if(params.password && params.password.length < 6) {
        return Response.failWithMessage(response, "User must have a password longer than 6 characters.");
    }

    return false;
}