var Response = require(process.cwd() + '/lib/response');
var Util = require(process.cwd() + '/lib/util');
var config = require(process.cwd() + '/config/config.js');
var bcrypt = require('bcrypt-nodejs');
var jwt = require('jwt-simple');
var fs = require('fs');

module.exports = function(server, authenticator, models) {
    var resource = '/auth';

    /*
     * Login
     */
    server.post(resource + "/login", function (request, response, next) {
        if(Util.validationResponse(['username', 'password'], request, response)) {
            return;
        }

        var params = {
            username:   Util.params(request.params.username),
            password:   Util.params(request.params.password)
        };

        models.user.find({ where: {username: params.username} }).complete(function(err, user) {
            if (err) {
                console.log("\nError: " + err);
                return Response.error(response, "There was a problem getting user.");
            }

            // No user
            if(user == null) {
                console.log("\nError: No user with username " + params.username + ".");
                return Response.failWithMessage(response, "Username or password incorrect.", 401);
            }

            // Wrong password
            if( !bcrypt.compareSync(params.password, user.password) ){
                console.log("\nError: Password incorrect.");
                return Response.failWithMessage(response, "Username or password incorrect."), 401;
            }

            // Remove password
            user.password = undefined;
            delete user.password;

            var token = Util.generateToken(user, config.auth.tokenExpiration);
            var sessionData = {
                userId: user.id,
                token: token
            }

            models.session.create(sessionData).complete(function(err, session) {
                if (err) {
                    console.log("\nError: " + err);
                    return Response.error(response, "There was a problem creating session.");
                }

                // Delete old sessions
                models.session.findAll( {where: ["userId = ? AND id != ?", session.userId, session.id]} ).complete(function(err, sessions) {
                    for(var i in sessions) {
                        (function(session) {

                            session.destroy();

                        })(sessions[i]);
                    }
                });

                var data = {
                    userId: session.userId,
                    sessionId: session.id,
                    token: session.token,
                    user: user
                }

                return Response.success(response, data);

            });

        });

    });

    /*
     * Logout
     */
    server.post(resource + "/logout", function (request, response, next) {
        if(Util.validationResponse(['sessionId'], request, response)) {
            return;
        }

        var params = {
            sessionId:   Util.params(request.params.sessionId)
        };

        // Delete sessions
        models.session.find({ where: {id: params.sessionId} }).complete(function(err, session) {
            if (err) {
                console.log("\nError: " + err);
                return Response.error(response, "There was a problem getting session.");
            }

            if(session == null) {
                return Response.failWithMessage(response, "Session not found.", 404);
            }

            session.destroy().on('success', function(s) {
                return Response.success(response, true);
            });
        });
    });

}