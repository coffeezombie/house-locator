var Response = require(process.cwd() + '/lib/response');
var Util = require(process.cwd() + '/lib/util');
var Config = require(process.cwd() + '/config/config');

var moment = require('moment');
var google = require('googleapis');

module.exports = function(server, authenticator, io, models) {
    var resource = '/calendar';
    var calendar = google.calendar('v3');
    var authed = false;

    server.get(resource + "/", function (request, response, next) {
        if(Util.validationResponse(['access_token', 'token_type', 'refresh_token', 'expiry_date'], request, response)) {
            return;
        }

        var params = {
            access_token:   Util.params(request.params.access_token),
            refresh_token:  Util.params(request.params.refresh_token),
            token_type:     Util.params(request.params.token_type),
            expiry_date:    Util.params(request.params.access_token)
        };

        var oAuthClient = new google.auth.OAuth2(Config.google.clientID, Config.google.clientSecret, Config.google.redirectURL);
        oAuthClient.setCredentials(params);

        var today = moment().format('YYY-MM-DD') + 'T';

        calendar.events.list({
            calendarId: Config.google.calendarId,
            maxResults: 100,
            timeMine: today + '00:00:00.000Z',
            timeMax: today + '23:59:59.000Z',
            auth: oAuthClient
        }, function(err, events) {
            if(err) {
                console.log("Error fetching events.");
                console.log(err);
                return Response.error(response, err);
            }

            console.log(events);
            return Response.success(response, events);
        });
    });

    server.get(resource + "/auth", function (request, response, next) {

        // If redirected from oAuth
        var code = Util.params(request.params.code);
        if(code) {
            // Get an access token based on our OAuth code
            oAuthClient.getToken(code, function(err, tokens) {
                if (err) {
                    console.log('Error authenticating')
                    console.log(err);
                    return Response.error(response, err);
                }
                console.log(tokens);
                return Response.success(response, tokens);

                //// Store our credentials and redirect back to our main page
                //oAuthClient.setCredentials(tokens);
                //authed = true;
                //return Response.success(response, true);
            });

            return;
        }

        // Generate an OAuth URL and redirect there
        var url = oAuthClient.generateAuthUrl({
            access_type: 'offline',
            scope: 'https://www.googleapis.com/auth/calendar.readonly'
        });

        console.log(url);
        return Response.success(response, url);
    });

}