var Response = require(process.cwd() + '/lib/response');
var Util = require(process.cwd() + '/lib/util');
var config = require(process.cwd() + '/config/config.js');

module.exports = function(server, authenticator, twilio) {
    var resource = '/sms';
    var lastResponse = 0;
    var msgData = {
        from: "testing from",
        body: "testing body"
    }

    /*
     * Create Message
     */
    server.post(resource + "/", function (request, response, next) {
        if(Util.validationResponse(['phone', 'messageType'], request, response)) {
            return;
        }

        var params = {
            phone: Util.params(request.params.phone),
            messageType: Util.params(request.params.messageType)
        }

        params.phone = Util.sanitizePhone(params.phone);

        if( params.phone == false ) {
            return Response.error(response, "Malformed phone number.");
        }

        if(lastResponse + config.twilio.rateLimit > new Date().getTime()) {
            return Response.failWithMessage(response, "Rate limit exceeded.", 429);
        } else {
            lastResponse = new Date().getTime();
        }

        if(typeof config.twilio.messages[params.messageType] == 'undefined') {
            var text = "The message type '" + params.messageType + "' is not defined. Available message types: " + Object.keys(config.twilio.messages).join(", ") + ".";
            return Response.fail(response, {
                messageType: text
            });
        }

        twilio.sms.messages.create({
            to: params.phone,
            from: config.twilio.twilioNumber,
            body: config.twilio.messages[params.messageType]
        }, function(error, message) {
            if (error) {
                console.log("\nError: " + error);
                return Response.error(response, "There was a problem sending the SMS.");
            }

            return Response.success(response, {
                to: message.to,
                from: message.from,
                body: message.body
            });
        });
    });

    /*
     * Reply to Messages
    http://stackoverflow.com/questions/21189348/twilio-forward-received-sms-to-url
    ^these are the request parameters returned from above
    https://www.twilio.com/docs/api/twiml/sms/twilio_request?utm_source=stackoverflow&utm_medium=jan2014&utm_campaign=stackathon
     */
    server.post("/smsResponse/", function (request, response, next) {
        if(Util.validationResponse(['from','body'], request, response)) {
            return;
        }

        var params = {
            from: Util.params(request.params.from),
            body: Util.params(request.params.body)
        }

        params.from = Util.sanitizePhone(params.from);

        if( params.phone == false ) {
            return Response.error(response, "Malformed phone number.");
        }
        /*
        if(lastResponse + config.twilio.rateLimit > new Date().getTime()) {
            return Response.failWithMessage(response, "Rate limit exceeded.", 429);
        } else {
            lastResponse = new Date().getTime();
        }
        */

        msgData.from = params.from;
        msgData.body = params.body;
        return Response.success(response, params)

    });
    /*
     * Reply to Messages
     * Get message body and from phone number from POST request above
     */
    server.get("/smsResponse/", function (request, response, next) {
        /*
        if(lastResponse + config.twilio.rateLimit > new Date().getTime()) {
            return Response.failWithMessage(response, "Rate limit exceeded.", 429);
        } else {
            lastResponse = new Date().getTime();
        }
        */
        return Response.success(response, msgData)
    });

};