var Response = require(process.cwd() + '/lib/response');
var Util = require(process.cwd() + '/lib/util');

module.exports = function(server, authenticator, io, models) {
    var resource = '/houses';

    /*
     * Get houses
     */
    server.get(resource + "/", function (request, response, next) {
        models.house.findAll({ include: [ models.employee ] }).complete(function(err, houses) {
            if (err) {
                console.log("\nError: " + err);
                return Response.error(response, "There was a problem getting houses.");
            }

            return Response.success(response, houses);
        });
    });

    /*
     * Get single house
     */
    server.get(resource + "/:id", function (request, response, next) {
        if(Util.validationResponse(['id'], request, response)) {
            return;
        }

        var params = {
            id:  Util.params(request.params.id)
        }

        models.house.find({ where: {id: params.id}, include: [ models.employee ] }).complete(function(err, house) {
            if (err) {
                console.log("\nError: " + err);
                return Response.error(response, "There was a problem getting house.");
            }

            return Response.success(response, house);
        });
    });

    /*
     * Create house
     */
    server.post(resource + "/", authenticator.authenticate, function (request, response, next) {
        if(Util.validationResponse(['x', 'y', 'angle', 'number', 'color'], request, response)) {
            return;
        }

        var params = {
            x:          Util.params(request.params.x),
            y:          Util.params(request.params.y),
            angle:      Util.params(request.params.angle),
            number:     Util.params(request.params.number),
            //width:      Util.params(request.params.width),
            //height:     Util.params(request.params.height),
            color:      Util.params(request.params.color),
            employeeId: Util.params(request.params.employeeId)
        }

        models.house.create(params).complete(function(err, house) {
            if (err) {
                console.log("\nError: " + err);
                return Response.error(response, "There was a problem creating house.");
            }

            models.employee.find({ where: {id: params.employeeId} }).complete(function(err, employee) {
                if(employee) {
                    employee.setHouse(house);
                }

                // io.emit('update', true);

                return Response.success(response, house);
            });
        });
    });

    /*
     * Update house
     */
    server.put(resource + "/:id", authenticator.authenticate, function (request, response, next) {
        //setTimeout(function() {

            if(Util.validationResponse(['id'], request, response)) {
                return;
            }

            var params = {
                id:         Util.params(request.params.id),
                x:          Util.params(request.params.x),
                y:          Util.params(request.params.y),
                angle:      Util.params(request.params.angle),
                number:     Util.params(request.params.number),
                //width:      Util.params(request.params.width),
                //height:     Util.params(request.params.height),
                color:      Util.params(request.params.color),
                employeeId: Util.params(request.params.employeeId)
            }

            models.house.find({ where: {id: params.id}, include: [ models.employee ]}).complete(function(err, house) {
                if (err) {
                    console.log("\nError: " + err);
                    return Response.error(response, "There was a problem getting house.");
                }

                house.update(params, {where: {id: params.id}}).complete(function(err) {
                    if(err) {
                        console.log("\nError: " + err);
                        return Response.error(response, "There was a problem saving house.");
                    }

                    models.employee.find({ where: {id: params.employeeId} }).complete(function(err, employee) {
                        if(employee) {
                            employee.setHouse(house);
                        }

                        // io.emit('update', true);

                        return Response.success(response, house);
                    });
                });
            });

        //}, 3000);
    });

    /*
     * Delete house
     */
    server.del(resource + "/:id", authenticator.authenticate, function (request, response, next) {

        if(Util.validationResponse(['id'], request, response)) {
            return;
        }

        var params = {
            id:  Util.params(request.params.id)
        }

        models.house.find({ where: {id: params.id}}).complete(function(err, house) {
            if (err) {
                console.log("\nError: " + err);
                return Response.error(response, "There was a problem getting house.");
            }

            house.destroy().complete(function(err) {
                if(err) {
                    console.log("\nError: " + err);
                    return Response.error(response, "There was a problem deleting house.");
                }

                // io.emit('update', true);

                return Response.success(response, true);
            });
        });

    });

}