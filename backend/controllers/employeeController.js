var Response = require(process.cwd() + '/lib/response');
var Util = require(process.cwd() + '/lib/util');
var fs = require('fs');

module.exports = function(server, authenticator, io, models) {
    var resource = '/employees';

    /*
     * Get employees
     */
    server.get(resource + "/", function (request, response, next) {
        models.employee.findAll({ include: [ models.house ] }).complete(function(err, employees) {
            if (err) {
                console.log("\nError: " + err);
                return Response.error(response, "There was a problem getting employees.");
            }

            return Response.success(response, employees);
        });
    });

    /*
     * Get single employee
     */
    server.get(resource + "/:id", function (request, response, next) {
        if(Util.validationResponse(['id'], request, response)) {
            return;
        }

        var params = {
            id:  Util.params(request.params.id)
        }

        models.employee.find({ where: {id: params.id}, include: [ models.house ] }).complete(function(err, employee) {
            if (err) {
                console.log("\nError: " + err);
                return Response.error(response, "There was a problem getting employee.");
            }

            return Response.success(response, employee);
        });
    });

    /*
     * Create employee
     */
    server.post(resource + "/", authenticator.authenticate, function (request, response, next) {
        if(Util.validationResponse(['firstName', 'lastName', 'position', 'phone', 'hours'], request, response)) {
            return;
        }

        var params = {
            firstName:  Util.params(request.params.firstName),
            lastName:   Util.params(request.params.lastName),
            position:   Util.params(request.params.position),
            phone:      Util.params(request.params.phone),
            hours:      Util.params(request.params.hours),
            houseId:    Util.params(request.params.houseId)
        }

        //if( Util.sanitizePhone(params.phone) == false ) {
        //    return Response.failWithMessage(response, "The phone number entered does not contain the correct amount of digits. Phone number must include area code, with an optional '1' at the beginning.");
        //}

        models.employee.create(params).complete(function(err, employee) {
            if (err) {
                console.log("\nError: " + err);
                return Response.error(response, "There was a problem creating employee.");
            }

            savePhoto(employee, request);

            employee.update({photoName: employee.photoName, photoUrl: employee.photoUrl}).complete(function(err) {
                if(err) {
                    console.log("\nError: " + err);
                    return Response.error(response, "There was a problem saving employee photo.");
                }

                models.house.find({ where: {id: params.houseId}, include: [ models.house ] }).complete(function(err, house) {
                    employee.setHouse(house);

                    // io.emit('update', true);

                    return Response.success(response, employee);
                });
            });
        });
    });

    /*
     * Update employee
     */
    server.put(resource + "/:id", authenticator.authenticate, function (request, response, next) {
        //setTimeout(function() {

        if(Util.validationResponse(['id'], request, response)) {
            return;
        }

        var params = {
            id:         Util.params(request.params.id),
            firstName:  Util.params(request.params.firstName),
            lastName:   Util.params(request.params.lastName),
            position:   Util.params(request.params.position),
            phone:      Util.params(request.params.phone),
            hours:      Util.params(request.params.hours),
            houseId:    Util.params(request.params.houseId)
        }

        models.employee.find({ where: {id: params.id}, include: [ models.house ] }).complete(function(err, employee) {
            if (err) {
                console.log("\nError: " + err);
                return Response.error(response, "There was a problem getting employee.");
            }
            if(employee === null) {
                return Response.error(response, "Couldn't find employee.");
            }

            employee.update(params, {where: {id: params.id}}).complete(function(err) {
                if(err) {
                    console.log("\nError: " + err);
                    return Response.error(response, "There was a problem saving employee.");
                }

                savePhoto(employee, request);

                employee.update({photoName: employee.photoName, photoUrl: employee.photoUrl}).complete(function(err) {
                    if(err) {
                        console.log("\nError: " + err);
                        return Response.error(response, "There was a problem saving employee photo.");
                    }

                    // io.emit('update', true);

                    return Response.success(response, employee);
                });
            });
        });

        //}, 3000);
    });


    /*
     * Delete employee
     */
    server.del(resource + "/:id", authenticator.authenticate, function (request, response, next)
    {
        if(Util.validationResponse(['id'], request, response)) {
            return;
        }

        var params = {
            id:  Util.params(request.params.id)
        }

        models.employee.find({ where: {id: params.id}}).complete(function(err, employee) {
            if (err) {
                console.log("\nError: " + err);
                return Response.error(response, "There was a problem getting employee.");
            }

            deletePhoto(employee);

            employee.destroy().complete(function(err) {
                if(err) {
                    console.log("\nError: " + err);
                    return Response.error(response, "There was a problem deleting employee.");
                }

                // io.emit('update', true);

                return Response.success(response, true);
            });
        });

    });

}

function savePhoto(employee, request) {
    if(typeof request.files == 'undefined') {
        return null;
    }

    if(!request.files.photo) {
        return null;
    }

    var photo = request.files.photo;
    console.log("Photo:", photo);
    var imageName = employee.firstName + "-" + employee.lastName + "-" + (new Date()).getTime() + ".jpg";
    if (!imageName) {
        console.log("There was an error getting photo from request.");
    }

    var data = fs.readFileSync(photo.path);
    if (!data) {
        console.log("There was an error reading photo.");
    }

    var newPath = process.cwd() + "/uploads/" + imageName;
    var writeData = fs.writeFileSync(newPath, data);

    if (typeof writeData !== 'undefined') {
        console.log("There was an error writing photo.");
    }

    var deleteResult = deletePhoto(employee);
    console.log("Photo deleted?: " + deleteResult);

    employee.photoName = imageName;
    employee.photoUrl = "/image/" + imageName;
}

function deletePhoto(employee) {
    if(employee.photoName) {
        var photoPath = process.cwd() + "/uploads/" + employee.photoName;

        try {
            fs.unlinkSync(photoPath);
        } catch(e) {
            console.log("Photo " + photoPath + " does not exist.");
        }
    }
}