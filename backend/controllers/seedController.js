var Response = require(process.cwd() + '/lib/response');
var Util = require(process.cwd() + '/lib/util');
var bcrypt = require('bcrypt-nodejs');

module.exports = function(server, authenticator, io, models) {
    var resource = '/seed';

    /*
     * Create data
     */
    server.post(resource + "/", function (request, response, next) {

        var house1 = {
            x:          0.1,
            y:          0.1,
            angle:      0,
            number:     "131",
            color:      'red'
            //employeeId: null
        }

        var house2 = {
            x:          0.3,
            y:          0.3,
            angle:      0,
            number:     "242",
            color:      'green'
        }

        var house3 = {
            x:          0.6,
            y:          0.6,
            angle:      20,
            number:     "343",
            color:      'blue'
        }

        var employee1 = {
            firstName:  'Joe',
            lastName:   'Shmoe',
            position:   'Web Developer',
            phone:      '(555) 555-5555',
            photoName:  'employee-1.jpg',
            photoUrl:   '/image/employee-1.jpg',
            hours:      'Mon - Fri: 8:00am - 5:00pm'
        }

        var employee2 = {
            firstName:  'Sam',
            lastName:   'Deere',
            position:   'iOS Developer',
            phone:      '(444) 444-4444',
            photoName:  'employee-2.jpg',
            photoUrl:   '/image/employee-2.jpg',
            hours:      'Mon - Fri: 7:00am - 4:00pm'
        }

        var employee3 = {
            firstName:  'Jane',
            lastName:   'Smith',
            position:   'Partner',
            phone:      '(333) 333-3333',
            photoName:  'employee-3.jpg',
            photoUrl:   '/image/employee-3.jpg',
            hours:      'Mon - Fri: 8:00am - 4:30pm'
        }

        var user = {
            username: 'admin',
            password: bcrypt.hashSync('password')
        }

        models.house.create(house1).complete(function(err, houseObject1) {
            models.employee.create(employee1).complete(function(err, employeeObject1) {
                employeeObject1.setHouse(houseObject1).complete(function(err) {

                    models.house.create(house2).complete(function(err, houseObject2) {
                        models.employee.create(employee2).complete(function(err, employeeObject2) {
                            employeeObject2.setHouse(houseObject2).complete(function(err) {

                                models.house.create(house3).complete(function (err, houseObject3) {
                                    models.employee.create(employee3).complete(function (err, employeeObject3) {
                                        employeeObject3.setHouse(houseObject3).complete(function(err) {

                                            models.user.create(user).complete(function(err, userObject) {

                                                if (err) {
                                                    return Response.error(response, "There was a problem creating data.");
                                                }

                                                // io.emit('update', true);

                                                return Response.success(response, {});

                                            });

                                        });
                                    });
                                });

                            });
                        });
                    });

                });
            });
        });

    });

}