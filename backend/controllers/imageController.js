var Response = require(process.cwd() + '/lib/response');
var Util = require(process.cwd() + '/lib/util');
var fs = require('fs');

module.exports = function(server, authenticator) {
    var resource = '/image';

    /*
     * Get image
     */
    server.get(resource + "/:filename", function (request, response, next) {
        var file = request.params.filename;
        var img = fs.readFileSync(process.cwd() + "/uploads/" + file);

        response.writeHead(200, {'Content-Type': 'image/jpeg' });
        response.end(img, 'binary');
    });

}