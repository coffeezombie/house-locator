module.exports = function(server, models, io, twilio) {
    var controllers = {};

    var authenticator = require(process.cwd() + '/middlewares/authenticator')(server, models);

    controllers.employee = require(process.cwd() + '/controllers/employeeController')(server, authenticator, io, models);
    controllers.house = require(process.cwd() + '/controllers/houseController')(server, authenticator, io, models);
    controllers.user = require(process.cwd() + '/controllers/userController')(server, authenticator, io, models);
    controllers.sms = require(process.cwd() + '/controllers/smsController')(server, authenticator, twilio);
    controllers.calendar = require(process.cwd() + '/controllers/calendarController')(server, authenticator, io, models);
    controllers.image = require(process.cwd() + '/controllers/imageController')(server, authenticator);
    controllers.auth = require(process.cwd() + '/controllers/authController')(server, authenticator, models);

    controllers.seed = require(process.cwd() + '/controllers/seedController')(server, authenticator, io, models);

    return controllers;
}