var Response = require(process.cwd() + '/lib/response');
var Util = require(process.cwd() + '/lib/util');
var jwt = require('jwt-simple');
var config = require(process.cwd() + '/config/config');

module.exports = function(server, models) {
    var authenticator = {};

    authenticator.authenticate = function(req, res, next) {
        console.log(req.params);

        if (Util.validationResponse(['sessionId', 'sessionUserId', 'sessionToken'], req, res)) {
            return;
        }

        var params = {
            sessionId: Util.params(req.params.sessionId),
            userId: Util.params(req.params.sessionUserId),
            token: Util.params(req.params.sessionToken)
        }

        // Skip auth for preflight OPTIONS requests.
        if (req.method == 'OPTIONS') {
            next();
        }

        models.session.find( {where: {id: params.sessionId}} ).complete(function (err, session) {
            if (err) {
                console.log("\nError: " + err);
                return Response.error(res, "There was a problem getting session.");
            }

            // No session with sessionId
            if(session == null) {
                console.log("\nError: No session.");
                return Response.failWithMessage(res, "Bad credentials.", 401);
            }

            // Decode token
            var decoded = jwt.decode(session.token, config.auth.secret);

            // Tokens don't match
            if (params.token != session.token) {
                console.log("\nError: Tokens don't match.");
                return Response.failWithMessage(res, "Bad credentials.", 401);
            }

            // Wrong userId in session
            if (session.userId != params.userId) {
                console.log("\nError: Wrong userId in session.");
                return Response.failWithMessage(res, "Bad credentials.", 401);
            }

            // Wrong userId in token
            if (decoded.userId != params.userId) {
                console.log("\nError: Wrong userId in token.");
                return Response.failWithMessage(res, "Bad credentials.", 401);
            }

            // Token expired
            if (decoded.expires <= Date.now()) {
                console.log("\nError: Token expired.");
                return Response.failWithMessage(res, "Token expired.", 401);
            }

            // Success
            next(); // Move to next middleware

        });
    }

    return authenticator;
}