var parameters = require('./parameters');

module.exports = {
    app: {
        name: 'house-tracker-api',
        port: '3001'
    },
    database: {
        name:           parameters.database.name,
        username:       parameters.database.username,
        password:       parameters.database.password,
        host:           'localhost',
        dialect:        'sqlite', // or 'sqlite', 'postgres', 'mariadb'
        port:           3306, // or 5432 (for postgres),
        storage:        process.cwd() + '/database.sqlite', // SQLite only,
        underscored:    true
    },
    twilio: {
        sid: parameters.twilio.sid,
        authToken: parameters.twilio.auth_token,
        twilioNumber: parameters.twilio.twilio_number,
        rateLimit: 1 * 60 * 1000, // 1 minute
        messages: {
            inLobby: "You have a visitor waiting in the lobby.",
            onMyWay: "You have a visitor on their way to the office."
        }
    },
    auth: {
        tokenExpiration: 7, // 7 days
        secret: parameters.auth.secret
    },
    google: {
        clientID: parameters.google.clientID,
        clientSecret: parameters.google.clientSecret,
        calendarId: 'developertown.com_2d3435393833343638323537@resource.calendar.google.com',
        redirectURL: 'http://localhost:3001/calendar/auth' // Frontend URL to redirect to after choosing an account to get calendars from
    }
};