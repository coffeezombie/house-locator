var Sequelize = require('sequelize');

module.exports = function(sequelize) {
    return sequelize.define('House', {
        x: Sequelize.INTEGER,
        y: Sequelize.INTEGER,
        angle: Sequelize.FLOAT,
        number: Sequelize.STRING,
        color: Sequelize.STRING
    });
}