var Sequelize = require('sequelize');

module.exports = function(sequelize) {
    return sequelize.define('Session', {
        userId: Sequelize.INTEGER,
        token: Sequelize.STRING
    });
}