var Sequelize = require('sequelize');

module.exports = function(sequelize) {
    return sequelize.define('User', {
        username: { type: Sequelize.STRING,  unique: 'compositeIndex'},
        password: Sequelize.STRING
    });
}