var Sequelize = require('sequelize');

module.exports = function(sequelize) {
    return sequelize.define('Employee', {
        firstName: Sequelize.STRING,
        lastName: Sequelize.STRING,
        phone: Sequelize.STRING,
        position: Sequelize.STRING,
        photoName: Sequelize.STRING,
        photoUrl: Sequelize.STRING,
        hours: Sequelize.TEXT
    });
}