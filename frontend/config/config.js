app.constant('CONFIG', {
    api: 'http://localhost:3001',
    messageTimeout: 5000
});

app.constant('SAVE_STATE', {
    saved: 1,
    unsaved: 2,
    saving: 3,
    error: 4
});

app.constant('CANVAS', {
    angleSnap: 22.5,
    keyMovementAmount: 5,
    keyMovementAmountSmall: 1,
    canvasRatio: 1.34, // Canvas width is 1.34 larger than house height
    houseRatio: 1.286, // House width is 1.286 larger than house height
    canvasToHouseHeightRatio: 16.031 // Canvas height is 16.031 larger than house height
});

app.constant('AUTH_EVENTS', {
    loginSuccess: 'auth-login-success',
    loginFailed: 'auth-login-failed',
    logoutSuccess: 'auth-logout-success',
    sessionTimeout: 'auth-session-timeout',
    notAuthenticated: 'auth-not-authenticated',
    notAuthorized: 'auth-not-authorized'
});

angular.module('houseLocatorApp').value('GoogleApp', {
    apiKey: 'AIzaSyB4MC1lU7jmpf5H6qRDaDH942FX_Z3Sgf0',
    clientId: '5695497471-ngfce2tsg8dknhkeg2n34l1071s362cr.apps.googleusercontent.com',
    scopes: [
      'https://www.googleapis.com/auth/calendar',
    ]
});