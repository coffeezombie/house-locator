app.filter('searchName', function() {
    return function(list, input) {
        // Has name
        list = list.filter(function(person) {
            return person.firstName + " " + person.lastName != " ";
        });

        if(typeof input === 'undefined') {
            return list;
        }

        // Match search
        return list.filter(function(person) {
            var name = person.firstName + " " + person.lastName;

            return name.toLowerCase().indexOf(input.toLowerCase()) > -1;
        });

    }
});

app.filter('searchUsername', function() {
    return function(list, input) {
        // Has username
        list = list.filter(function(user) {
            return user.username != " ";
        });

        if(typeof input === 'undefined') {
            return list;
        }

        // Match search
        return list.filter(function(user) {
            return user.username.toLowerCase().indexOf(input.toLowerCase()) > -1;
        });

    }
});