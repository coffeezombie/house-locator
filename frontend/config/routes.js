app.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('');

    $stateProvider
        // Site
        .state("site", {
            abstract: false,
            url: "",
            templateUrl: "views/Site/main.html" })
        .state("site.search", {
            url: "/",
            controller: "SearchController",
            templateUrl: "views/Site/search.html" })
        .state("site.schedule", {
            url: "/schedule",
            controller: "ScheduleController",
            templateUrl: "views/Site/schedule.html" })
        .state("site.employee", {
            url: "/employee/:id",
            controller: "EmployeeController",
            templateUrl: "views/Site/employee.html" })

        // Admin
        .state("admin", {
            abstract: false,
            url: "/admin",
            templateUrl: "views/Admin/main.html" })
        .state("admin.employees", {
            url: "/employees",
            controller: "SearchController",
            templateUrl: "views/Admin/employees.html" })
        .state("admin.employee", {
            url: "/employee/:id",
            controller: "AdminEmployeeController",
            templateUrl: "views/Admin/employee.html" })
        .state("admin.users", {
            url: "/users",
            controller: "AdminUsersController",
            templateUrl: "views/Admin/users.html" })
        .state("admin.user", {
            url: "/user/:id",
            controller: "AdminUserController",
            templateUrl: "views/Admin/user.html" })

        .state("login", {
            url: "/login",
            controller: "LoginController",
            templateUrl: "views/login.html" })
    ;
});