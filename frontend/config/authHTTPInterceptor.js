app.config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push(function($rootScope, $q, $location, AUTH_EVENTS, Session)
    {
        var interceptor = {};

        interceptor.request = function(config) {
            if( !!Session.user ) {
                if(config.method === 'POST') {
                    config.data.sessionToken = Session.token;
                    config.data.sessionId = Session.id;
                    config.data.sessionUserId = Session.user.id;
                } else {
                    config.params = {};
                    config.params.sessionToken = Session.token;
                    config.params.sessionId = Session.id;
                    config.params.sessionUserId = Session.user.id;
                }

                //console.log("Modified request config:", config);
            }

            return config;
        }

        interceptor.requestError = function(rejection) {
            console.log("Request Error:", rejection);
            return $q.reject(rejection);
        }

        interceptor.response = function(response) {
            return response;
        }

        interceptor.responseError = function(rejection) {
            // Unauthorized
            if(rejection.status === 401) {
                $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
            }

            return $q.reject(rejection);
        }

        return interceptor;
    });
}]);