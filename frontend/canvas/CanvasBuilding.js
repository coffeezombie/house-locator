var CanvasBuilding = fabric.util.createClass(fabric.Image, {
    type: 'building',
    defaultImage: 'house-image',

    initialize: function(options) {
        this.callSuper('initialize', options);

        options.canvas = options.canvas ? options.canvas : null;
        options.id = options.id ? options.id : null;
        options.employeeId = options.employeeId ? options.employeeId : null;
        options.imageElement = options.imageElement ? options.imageElement : this.defaultImage;

        options.percentX = options.percentX || 0;
        options.percentY = options.percentY || 0;

        this.set(options);

        this.setElement( document.getElementById(options.imageElement) );
        this.set('label', options.label || '');
        this.animating = false;

        this.updateScale();

        this.set({
            lockScalingX: true,
            lockScalingY: true,
            originX: 'origin',
            originY: 'origin'
        });

        this.events();
    },

    updateScale: function() {
        this.setHeight( Math.round(this.canvas.getHeight() / this.canvas.canvasConfig.canvasToHouseHeightRatio) );
        this.setWidth( Math.round(this.getHeight() * this.canvas.canvasConfig.houseRatio) );
        this.set({
            left: Math.round(this.canvas.getWidth() * this.percentX),
            top: Math.round(this.canvas.getHeight() * this.percentY)
        });
    },

    getPositionPercentages: function() {
        var percentageX = this.getLeft() / this.canvas.getWidth();
        var percentageY = this.getTop() / this.canvas.getHeight();
        return {
            x: percentageX,
            y: percentageY
        }
    },

    updatePositionPercentages: function()
    {
        this.percentX = this.getPositionPercentages().x;
        this.percentY = this.getPositionPercentages().y;
    },

    toObject: function() {
        return fabric.util.object.extend(this.callSuper('toObject'), {
            //label: this.get('label')
        });
    },

    events: function() {
        this.on({
            'object:moving': function(e) {
                console.log("moving");
            },
            'object:rotating': function(e) {
                console.log("rotating");
            },
            'object:modified': function(e) {

            }
        });
    },

    bounce: function(canvas, direction, distance) {
        var object = this;
        var direction = direction || 'top';
        var distance = distance || 40;

        var jumpDuration = 400;
        var fallDuration = 800;

        if(this.animating) {
            return;
        }

        this.animating = true;
        object.animate(direction, object.getTop() - distance, {
            duration: jumpDuration,
            onChange: canvas.renderAll.bind(canvas),
            onComplete: function() {

                object.animate(direction, object.getTop() + distance, {
                    duration: fallDuration,
                    onChange: canvas.renderAll.bind(canvas),
                    onComplete: function() {
                        object.animating = false;
                    },
                    easing: fabric.util.ease.easeOutBounce
                });

            },
            easing: fabric.util.ease.easeOutQuad
        });
    },

    _render: function(ctx) {
        this.callSuper('_render', ctx);

        var textAngle = -this.angle * (Math.PI/180);
        var textHeight = this.height / 3;
        var textX = -this.height/3.6;
        var textY = textHeight/3;

        ctx.rotate(textAngle);
        ctx.font = textHeight + 'px Roboto';
        ctx.fillStyle = '#fff';
        ctx.fillText(this.label, textX, textY);
    }
});



/* ======================================================== */
/* Static
/* ======================================================== */

var CanvasStaticBuilding = fabric.util.createClass(CanvasBuilding, {
    type: 'staticBuilding',

    initialize: function(options) {
        options || (options = {});
        this.callSuper('initialize', options);
        this.set('lockMovementX', true);
        this.set('lockMovementY', true);
        this.set('lockScalingX', true);
        this.set('lockScalingY', true);
        this.set('lockRotation', true);
        this.set('hasControls', false);
    },

    toObject: function() {
        return fabric.util.object.extend(this.callSuper('toObject'));
    },

    _render: function(ctx) {
        this.callSuper('_render', ctx);
    }
});

