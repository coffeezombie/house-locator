var YouAreHereIndicator = fabric.util.createClass(fabric.Image, {
    type: 'youAreHereIndicator',
    defaultImage: 'you-are-here-indicator',

    initialize: function(options) {
        this.callSuper('initialize', options);

        options.canvas = options.canvas ? options.canvas : null;
        options.imageElement = options.imageElement ? options.imageElement : this.defaultImage;

        options.percentX = options.percentX || 0;
        options.percentY = options.percentY || 0;

        this.set(options);

        this.setElement( document.getElementById(options.imageElement) );
        this.set('label', options.label || '');

        this.updateScale();

        this.set({
            lockScalingX: true,
            lockScalingY: true,
            lockRotation: true,
            originX: 'origin',
            originY: 'origin'
        });

        this.events();
    },

    updateScale: function() {
        this.setHeight(this.canvas.getHeight() / this.canvas.canvasConfig.canvasToHouseHeightRatio);
        this.setWidth(this.getHeight());
        this.set({
            left: (this.canvas.getWidth() * this.percentX),
            top: (this.canvas.getHeight() * this.percentY)
        });
    },

    getPositionPercentages: function() {
        var percentageX = this.getLeft() / this.canvas.getWidth();
        var percentageY = this.getTop() / this.canvas.getHeight();
        return {
            x: percentageX,
            y: percentageY
        }
    },

    updatePositionPercentages: function()
    {
        this.percentX = this.getPositionPercentages().x;
        this.percentY = this.getPositionPercentages().y;
    },

    toObject: function() {
        return fabric.util.object.extend(this.callSuper('toObject'), {
            //label: this.get('label')
        });
    },

    events: function() {
        this.on({
            'object:moving': function(e) {
                console.log("moving");
            },
            'object:rotating': function(e) {
                console.log("rotating");
            },
            'object:modified': function(e) {
                
            }
        });
    },

    _render: function(ctx) {
        this.callSuper('_render', ctx);

        ctx.font = '14px Helvetica';
        ctx.fillStyle = '#333';
        ctx.fillText(this.label, -15, 7);
    }
});

/* ======================================================== */
/* Static
/* ======================================================== */

var StaticYouAreHereIndicator = fabric.util.createClass(YouAreHereIndicator, {
    type: 'staticYouAreHereIndicator',

    initialize: function(options) {
        options || (options = {});
        this.callSuper('initialize', options);
        this.set('selectable', false);
    },

    toObject: function() {
        return fabric.util.object.extend(this.callSuper('toObject'));
    },

    _render: function(ctx) {
        this.callSuper('_render', ctx);
    }

});