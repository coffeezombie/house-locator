var Canvas = fabric.util.createClass(fabric.Canvas, {
    type: 'myCanvas',

    initialize: function(el, options) {
        this.callSuper('initialize', el, options);
        this.canvasConfig = options.canvasConfig;
        this.canvasContainer = options.canvasContainer;
        this.houseUpdateCallback = options.houseUpdateCallback || null;
        this.houseSelectCallback = options.houseSelectCallback || null;
        this.houseDeselectCallback = options.houseDeselectCallback || null;

        this.events(this);
        this.autoSize(this);

        this.setBackgroundImage('assets/images/bg.jpg', this.renderAll.bind(this), {
            left: 0,
            top: 0,
            width: this.getWidth(),
            height: this.getHeight(),
            originX: 'left',
            originY: 'top'
        });

        this.selectionColor = 'rgba(0,255,0,0.3)';
        this.selectionBorderColor = 'red';
        this.selectionLineWidth = 5;
    },

    toObject: function() {
        return fabric.util.object.extend(this.callSuper('toObject'), {
            canvasContainer: this.canvasContainer
        });
    },

    initYouAreHereIndicator: function(x, y, editable) {
        var youAreHereIndicator = null;
        var options = {
            canvas: this,
            imageElement: 'you-are-here-indicator',
            percentX: x,
            percentY: y
        };

        if(editable) {
            youAreHereIndicator = new YouAreHereIndicator(options);
        } else {
            youAreHereIndicator = new StaticYouAreHereIndicator(options);
        }

        this.add(youAreHereIndicator);
    },

    events: function(canvas) {
        this.on({
            'object:added': function(e) {
                var object = e.target;

                object.borderColor = '#1f78b3';
                object.borderSize = 4;
                object.cornerColor = '#1f78b3';

                var randScale = 1.5 + (Math.random() * 1.1);
                var randOpacity = Math.random() * .2;
                var randScaleTime = 200 + (Math.random() * 1500);
                var randOpacityTime = 400 + (Math.random() * 1000);

                object.opacity = randOpacity;
                object.scaleX = randScale;
                object.scaleY = randScale;

                object.animate('opacity', 1, {
                    onChange: this.renderAll.bind(this),
                    duration: randOpacityTime,
                    easing: fabric.util.ease.easeOutQuart
                });

                object.animate('scaleX', 1, {
                    onChange: this.renderAll.bind(this),
                    duration: randScaleTime,
                    easing: fabric.util.ease.easeOutQuart
                });

                object.animate('scaleY', 1, {
                    onChange: this.renderAll.bind(this),
                    duration: randScaleTime,
                    easing: fabric.util.ease.easeOutQuart
                });
            },
            'object:moving': function(e) {
                var object = e.target;
                this.snapToPixel(object);
                this.preventOutOfBounds(object);
                object.updatePositionPercentages();
                if(this.houseUpdateCallback) {
                    this.houseUpdateCallback(object);
                }
            },
            'object:rotating': function(e) {
                var object = e.target;
                this.snapToAngle(object, canvas.canvasConfig.angleSnap);
                this.preventOutOfBounds(object);
                object.updatePositionPercentages();
                if(this.houseUpdateCallback) {
                    this.houseUpdateCallback(object);
                }
            },
            'object:modified': function(e) {
                var object = e.target;
            },
            'object:selected': function(e) {
                var object = e.target;

                if(this.houseSelectCallback) {
                    this.houseSelectCallback(object);
                }
            },
            'before:selection:cleared': function(e) {
                var object = e.target;
                if(this.houseDeselectCallback) {
                    this.houseDeselectCallback(object);
                }
            }
        });

        var autoSize = this.autoSize;
        $(window).resize(function() {
            autoSize(canvas);
        });


        var keyDown = this.keyDown;
        var keyUp = this.keyUp;
        var canvasWrapper = document.getElementById('canvas-container');
        canvasWrapper.tabIndex = 1000;
        canvasWrapper.addEventListener("keydown", function(e) {
            keyDown(e, canvas);
        }, false);

        canvasWrapper.addEventListener("keyup", function(e) {
            keyUp(e, canvas);
        }, false);
    },

    keyDown: function(e, canvas) {
        var activeObject = canvas.getActiveObject() ? canvas.getActiveObject() : canvas.getActiveGroup();

        // Hold shift
        if(typeof canvas.shiftHeld == 'undefined') {
            canvas.shiftHeld = false;
        }
        if(e.keyCode == 16) {
            canvas.shiftHeld = true;
        }

        if(!activeObject) {
            return;
        }

        // Move object
        var movement = canvas.shiftHeld ? canvas.canvasConfig.keyMovementAmountSmall : canvas.canvasConfig.keyMovementAmount;
        switch (e.keyCode) {
            case 38:  /* Up arrow was pressed */
                var newPos = activeObject.top - movement;
                activeObject.top = newPos;
                break;
            case 40:  /* Down arrow was pressed */
                var newPos = activeObject.top + movement;
                activeObject.top = newPos;
                break;
            case 37:  /* Left arrow was pressed */
                var newPos = activeObject.left - movement;
                activeObject.left = newPos;
                break;
            case 39:  /* Right arrow was pressed */
                var newPos = activeObject.left + movement;
                activeObject.left = newPos;
                break;
        }

        activeObject.setCoords();
        canvas.renderAll();
    },

    keyUp: function(e, canvas) {
        // Hold shift
        if(typeof canvas.shiftHeld == 'undefined') {
            canvas.shiftHeld = false;
        }
        if(e.keyCode == 16) {
            canvas.shiftHeld = false;
        }
    },

    autoSize: function(canvas) {
        // Make sure function runs after full page load
        $(window).load(function() {
            canvas.sidePanelScroll(canvas);
        });

        // Canvas size
        var windowWidth = $(window).width();
        var height = $(window).height();
        var canvasNewWidth = Math.round(height * canvas.canvasConfig.canvasRatio);

        $('#canvas-column').width(canvasNewWidth);
        $('#side-panel-column').height(height).width(windowWidth - canvasNewWidth); // Don't know why + 15....

        canvas.setWidth( canvasNewWidth );
        canvas.setHeight( height );

        if(canvas.backgroundImage) {
            canvas.backgroundImage.height = height;
            canvas.backgroundImage.width = canvasNewWidth;
        }

        // Object sizes
        var objects = canvas.getObjects();
        for(var i in objects) {
            var object = objects[i];
            object.updateScale();
        }
    },

    sidePanelScroll: function(canvas) {
        var height = $(window).height();
        var padding = parseInt($('#side-panel-container').css("padding-top"), 10) + parseInt($('#side-panel-container').css("padding-bottom"), 10);
        $('#side-panel-container').height(height - $("#header-main").outerHeight(true) - padding);
    },

    preventOutOfBounds: function(obj) {
        obj.setCoords(); // This sets the coordinats for the bounding rect.
        var bound = obj.getBoundingRect();

        var leftDiff = obj.left - bound.left;
        var topDiff = obj.top - bound.top;

        // Object can't move off the canvas
        if(bound.left <= 0) {
            obj.left = leftDiff;
        } else if(bound.left + bound.width >= this.canvasContainer.innerWidth()) {
            obj.left = this.canvasContainer.innerWidth() - (bound.width - leftDiff);
        }

        if(bound.top <= 0) {
            obj.top = topDiff;
        } else if(bound.top + bound.height >= this.canvasContainer.innerHeight()) {
            obj.top = this.canvasContainer.innerHeight() - (bound.height - topDiff);
        }
    },

    snapToAngle: function(obj, snapAngle) {
        var newAngle = Math.round(obj.angle / snapAngle) * snapAngle;
        obj.setAngle(newAngle).setCoords();
    },

    snapToPixel: function(obj) {
        obj.left = Math.round(obj.left);
        obj.top = Math.round(obj.top);
        obj.setCoords();
    },

    _render: function(ctx) {
        this.callSuper('_render', ctx);
    }

});