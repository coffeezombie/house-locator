app.controller('ApplicationController', function ($rootScope, $scope, $state, CONFIG, HouseService)
{
    $scope.currentUser = null;

    $scope.setCurrentUser = function (user) {
        $scope.currentUser = user;
    };

    $rootScope.isState = function(stateName)
    {
        return $state.is(stateName);
    }

    $rootScope.bounceHouse = function(houseId)
    {
        HouseService.bounceCanvasHouse($scope.canvas, houseId);
    }

    $rootScope.getEmployeePhoto = function(employee)
    {
        var url = "assets/images/photo-default.jpg";

        if(employee.hasOwnProperty('photoUrl') && employee.photoUrl !== null) {
            url = CONFIG.api + employee.photoUrl;
        }

        return url;
    }

});