app.controller('AdminCanvasController', function($rootScope, $scope, $state, CONFIG, SAVE_STATE, CANVAS, messageCenterService, AuthService, SocketService, House, HouseService, Employee, EmployeeService, BatchService)
{
    function init()
    {
        if( AuthService.redirectOnUnauthorized() ) {
            return;
        }

        $rootScope.canvas = new Canvas('admin-canvas', {
            canvasConfig: CANVAS,
            canvasContainer: $('#canvas-container'),
            hoverCursor: 'pointer',
            selection: false,
            houseUpdateCallback: $scope.canvasHouseChanged,
            houseSelectCallback: $scope.canvasHouseSelected,
            houseDeselectCallback: $scope.canvasHouseDeselected
        });

        $scope.SAVE_STATE = SAVE_STATE;
        $scope.saveState = {};
        setSaveState(SAVE_STATE.saved, null);

        //$scope.houses.$promise.then(function(houses) {
        //    $rootScope.canvasHouses = HouseService.addHousesToCanvas(houses, $rootScope.canvas, true);
        //});
    }


    /* ======================================================== *
     * Public Functions
     * ======================================================== */

    $scope.saveHouses = function()
    {
        setSaveState(SAVE_STATE.saving);

        $scope.houses = HouseService.updateHousesFromCanvasHouses($scope.houses, $scope.canvasHouses);

        BatchService.batchUpdate(House, $scope.houses, function(data, errors) {
            if(errors) {
                console.log(errors);
                setSaveState(SAVE_STATE.error);
                return;
            }

            SocketService.socket.emit('update', true);

            setSaveState(SAVE_STATE.saved);
        });
    }

    $scope.addHouse = function() {
        var employee = new Employee({
            firstName: "",
            lastName: "",
            position: "",
            phone: "",
            photo: null,
            hours: ""
        });

        var house = new House({
            x: 0.5,
            y: 0.5,
            angle: 0,
            number: "000",
            color: null
        });

        Employee.save(employee, function (e) {
            house.employeeId = e.id;

            House.save(house, function (h) {
                House.get({id: h.id}).$promise.then(function (h) {
                    Employee.get({id: e.id}).$promise.then(function (e) {

                        var canvasHouses = HouseService.addHousesToCanvas([h], $scope.canvas, true);
                        e.photo = $scope.getEmployeePhoto(e);

                        $scope.employees.push(e);
                        $scope.houses.push(h);
                        $scope.canvasHouses.push(canvasHouses[0]);

                        SocketService.socket.emit('update', true);

                    });
                });
            });
        });
    }

    $scope.removeHouse = function(canvasHouse)
    {
        var houseId = canvasHouse.id;
        var employeeId = canvasHouse.employeeId;

        House.delete({id: houseId}, function(houseResponse) {
            $scope.canvas.remove(canvasHouse);
            $scope.canvas.renderAll();

            Employee.delete({id: employeeId}, function(employeeResponse) {
                SocketService.socket.emit('update', true);

                messageCenterService.add('sucess', "House removed successfully.", { timeout: CONFIG.messageTimeout });
            }, function(error) {
                console.log(error);
                messageCenterService.add('danger', "There was a problem removing employee for the house.", { timeout: CONFIG.messageTimeout });
            });
        }, function(error) {
            console.log(error);
            messageCenterService.add('danger', "There was a problem removing the house.", { timeout: CONFIG.messageTimeout });
        });
    }

    $scope.canvasHouseChanged = function(canvasHouse)
    {
        if(typeof $scope.lastChange === 'undefined') {
            $scope.changeTimeout = 300;
            $scope.lastChange = new Date().getTime() - $scope.changeTimeout;
            $scope.$apply();
        }

        if($scope.lastChange + $scope.changeTimeout > new Date().getTime()) {
            return;
        }

        $scope.lastChange = new Date().getTime();
        setSaveState(SAVE_STATE.unsaved);
        $scope.$apply();
    }

    $scope.canvasHouseSelected = function(canvasHouse)
    {
        $scope.activeHouse = canvasHouse;

        console.log(canvasHouse);

        $state.go('admin.employee', {id: canvasHouse.employeeId});
    }

    $scope.canvasHouseDeselected = function(canvasHouse)
    {
        $scope.activeHouse = null;
        $state.go('admin.employees');
    }

    $scope.setHousesSelectable = function(selectable)
    {
        for(var i in $scope.canvasHouses) {
            $scope.canvasHouses[i].selectable = selectable;
        }
    }


    /* ======================================================== *
     * Private Functions
     * ======================================================== */

    function setSaveState(state, date)
    {
        $scope.saveState = {
            state: state,
            date: (typeof date === 'undefined') ? new Date() : date
        }

        if(state === SAVE_STATE.saving) {
            $scope.setHousesSelectable(false);
        } else {
            $scope.setHousesSelectable(true);
        }

        if(state === SAVE_STATE.saving) {
            bindWindowUnload("You are in the proccess of saving. Leaving the page may interrupt that process.");
        } else if(state !== SAVE_STATE.saved) {
            bindWindowUnload("You have unsaved changes. If you leave the page, your unsaved changes will be lost.");
        } else {
            unbindWindowUnload();
        }
    }

    function bindWindowUnload(text)
    {
        $(window).on("beforeunload", function() {
            return text;
        });
    }

    function unbindWindowUnload()
    {
        $(window).unbind("beforeunload");
    }

    function randomId()
    {
        var length = 10;
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for(var i = 0; i < length; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }

        return text;
    }

    function isInt(n)
    {
        return Number(n) === n && n % 1 === 0;
    }


    /* ======================================================== *
     * Init
     * ======================================================== */

    init();

 });