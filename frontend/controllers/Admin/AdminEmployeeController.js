app.controller('AdminEmployeeController', function($rootScope, $scope, $state, $stateParams, CONFIG, SAVE_STATE, messageCenterService, AuthService, SocketService, EmployeeService, HouseService, Employee, House)
{
    function init()
    {
        if( AuthService.redirectOnUnauthorized() ) {
            return;
        }

        var id = $stateParams.id;

        $scope.SAVE_STATE = SAVE_STATE;
        $scope.saveState = {};
        setSaveState(SAVE_STATE.saved, null);

        $scope.employees.$promise.then(function() {
            $scope.employee = $scope.employees.filter(function(employee) {
                return employee.id == id;
            })[0];

            if($scope.employee === null || typeof $scope.employee === 'undefined') {
                $state.go('admin.employees');
                return;
            }

            HouseService.selectCanvasHouseFromHouseId($scope.canvas, $scope.employee.House.id);

            $scope.$watch('employee', function(newVal, oldVal) {
                if(newVal == oldVal) {
                    return;
                }

                $scope.employeeChanged();
            });
        });
    }


    /* ======================================================== *
     * Public Functions
     * ======================================================== */

    $scope.saveEmployee = function()
    {
        setSaveState(SAVE_STATE.saving);

        var employee = $.extend(true, {}, $scope.employee);
        if($scope.employeePhoto) {
            employee.photo = $scope.employeePhoto;
        }

        EmployeeService.update(employee, function(employeeData) {
            $scope.employee.photo = $scope.getEmployeePhoto(employeeData);

            var house = _.find($scope.houses, function(h) {
                return h.id == employee.House.id;
            });

            house.number = employee.House.number;

            House.update(house, function(houseData) {
                var canvasHouse = _.find($scope.canvasHouses, function(ch) {
                    return ch.id == houseData.id;
                });

                canvasHouse.label = houseData.number;
                $scope.canvas.renderAll();

                SocketService.socket.emit('update', true);

                messageCenterService.add('success', "Employee saved successfully.", { timeout: CONFIG.messageTimeout });
                setSaveState(SAVE_STATE.saved);
            }, function(errorData) {
                messageCenterService.add('danger', "There was a problem saving the house for the employee.", { timeout: CONFIG.messageTimeout });
                setSaveState(SAVE_STATE.error);
            });

            //var canvasHouse = $rootScope.canvasHouses.filter(function(canvasHouse) {
            //    return canvasHouse.employeeId == employee.id;
            //})[0];

            //canvasHouse.set({label: employee.firstName + " " + employee.lastName});
            //$rootScope.canvas.setActiveObject(canvasHouse); // This forces canvas to update rendered label
        }, function(error) {
            console.log(error);
            messageCenterService.add('danger', "There was a problem saving the employee.", { timeout: CONFIG.messageTimeout });
            setSaveState(SAVE_STATE.error);
        });
    }

    $scope.employeeChanged = function()
    {
        setSaveState(SAVE_STATE.unsaved);
    }


    /* ======================================================== *
     * Private Functions
     * ======================================================== */

    function setSaveState(state, date)
    {
        $scope.saveState = {
            state: state,
            date: (typeof date === 'undefined') ? new Date() : date
        }

        if(state === SAVE_STATE.saving) {
            $('.side-panel input').prop('disabled', true);
        } else {
            $('.side-panel input').prop('disabled', false);
        }

        if(state === SAVE_STATE.saving) {
            bindWindowUnload("You are in the proccess of saving. Leaving the page may interrupt that process.");
        } else if(state !== SAVE_STATE.saved) {
            bindWindowUnload("You have unsaved changes. If you leave the page, your unsaved changes will be lost.");
        } else {
            unbindWindowUnload();
        }
    }

    function bindWindowUnload(text)
    {
        $(window).on("beforeunload", function() {
            return text;
        });
    }

    function unbindWindowUnload()
    {
        $(window).unbind("beforeunload");
    }


    /* ======================================================== *
     * Init
     * ======================================================== */

    init();

});