app.controller('AdminMainController', function ($rootScope, $scope, $state, $location, CONFIG, CANVAS, AUTH_EVENTS, messageCenterService, Session, SocketService, AuthService, House, Employee, HouseService, User)
{
    function init()
    {
        if( AuthService.redirectOnUnauthorized() ) {
            return;
        }

        $scope.getData();
        listenForUpdates();

        $scope.$on(AUTH_EVENTS.notAuthenticated, function(event, args) {
            Session.destroy();
            $location.path('login');
            messageCenterService.add('danger', "Your session either does not exist or has expired. Please login again.");
        });
    }


    /* ======================================================== *
     * Public Functions
     * ======================================================== */

    $rootScope.logout = function(redirectState, redirectParams)
    {
        if(typeof redirectState === 'undefined') {
            redirectState = "site.search";
        }

        AuthService.logout().success(function() {
            $state.go(redirectState, redirectParams);
        });
    }

    $rootScope.getData = function()
    {
        $rootScope.houses = House.query(function(houses) {
            //HouseService.updateCanvasHousesFromHouses($scope.canvasHouses, houses);

            $scope.canvas.clear();
            $rootScope.canvasHouses = HouseService.addHousesToCanvas(houses, $rootScope.canvas, true);

            $scope.canvas.initYouAreHereIndicator(0.36, 0.9, true);

            $rootScope.canvas.renderAll();
        });
        $rootScope.users = User.query();
        $rootScope.employees = Employee.query(function(employees) {
            for(var i in employees) {
                var employee = employees[i];

                employee.photo = $scope.getEmployeePhoto(employee);
            }
        });
    }


    /* ======================================================== *
     * Private Functions
     * ======================================================== */

    function listenForUpdates()
    {
        SocketService.socket.on('update', function() {
            $scope.getData();
        });
    }


    /* ======================================================== *
     * Init
     * ======================================================== */

    init();

});