app.controller('AdminUsersController', function ($scope, User)
{
    function init()
    {

    }


    /* ======================================================== *
     * Public Functions
     * ======================================================== */

    $scope.clearSearch = function()
    {
        $scope.userSearch = "";
    }


    /* ======================================================== *
     * Init Functions
     * ======================================================== */

    init();

});