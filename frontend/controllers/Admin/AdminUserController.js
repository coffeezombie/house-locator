app.controller('AdminUserController', function($rootScope, $scope, $state, $stateParams, CONFIG, SAVE_STATE, messageCenterService, AuthService, SocketService, User)
{

    function init()
    {
        if( AuthService.redirectOnUnauthorized() ) {
            return;
        }

        var id = $stateParams.id;

        $scope.SAVE_STATE = SAVE_STATE;
        $scope.saveState = {};
        setSaveState(SAVE_STATE.saved, null);

        $scope.users.$promise.then(function() {
            $scope.user = $scope.users.filter(function(u) {
                return u.id == id;
            })[0];

            if($scope.user === null || typeof $scope.user === 'undefined') {
                $state.go('admin.users');
                return;
            }

            $scope.$watch('user', function(newVal, oldVal) {
                if(newVal == oldVal) {
                    return;
                }

                $scope.userChanged();
            }, true);
        });
    }


    /* ======================================================== *
     * Public Functions
     * ======================================================== */

    $scope.saveUser = function(user)
    {
        setSaveState(SAVE_STATE.saving);

        // Quick and dirty password validation...
        if(user.changePassword || user.confirmPassword) {
            if(user.changePassword !== user.confirmPassword) {
                messageCenterService.add('danger', "Password do not match.", { timeout: CONFIG.messageTimeout });
                setSaveState(SAVE_STATE.unsaved);
                return;
            }

            if(user.changePassword.length < 6) {
                messageCenterService.add('danger', "Password much be at least 6 characters long.", { timeout: CONFIG.messageTimeout });
                setSaveState(SAVE_STATE.unsaved);
                return;
            }

            user.password = user.changePassword;
        }

        User.update({id: user.id}, user, function(u) {
            messageCenterService.add('success', "User has been saved successfully.", { timeout: CONFIG.messageTimeout });
            setSaveState(SAVE_STATE.saved);
        }, function(error) {
            console.log(error);
            messageCenterService.add('danger', "There was a problem saving the user.", { timeout: CONFIG.messageTimeout });
            setSaveState(SAVE_STATE.error);
        });
    }

    $scope.userChanged = function()
    {
        setSaveState(SAVE_STATE.unsaved);
    }


    /* ======================================================== *
     * Private Functions
     * ======================================================== */

    function setSaveState(state, date)
    {
        $scope.saveState = {
            state: state,
            date: (typeof date === 'undefined') ? new Date() : date
        }

        if(state === SAVE_STATE.saving) {
            $('.side-panel input').prop('disabled', true);
        } else {
            $('.side-panel input').prop('disabled', false);
        }

        if(state === SAVE_STATE.saving) {
            bindWindowUnload("You are in the proccess of saving. Leaving the page may interrupt that process.");
        } else if(state !== SAVE_STATE.saved) {
            bindWindowUnload("You have unsaved changes. If you leave the page, your unsaved changes will be lost.");
        } else {
            unbindWindowUnload();
        }
    }

    function bindWindowUnload(text)
    {
        $(window).on("beforeunload", function() {
            return text;
        });
    }

    function unbindWindowUnload()
    {
        $(window).unbind("beforeunload");
    }


    /* ======================================================== *
     * Init
     * ======================================================== */

    init();

});