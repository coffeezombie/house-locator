app.controller('SearchController', function ($rootScope, $scope, HouseService, Employee)
{
    function init()
    {
        $scope.$on('$viewContentLoaded', pageLoad);
    }

    function pageLoad()
    {
        // Reset canvas selection
        $scope.canvas.deactivateAll().renderAll();
    }


    /* ======================================================== *
     * Public Functions
     * ======================================================== */

    $scope.clearSearch = function()
    {
        $scope.employeeSearch = "";
    }


    /* ======================================================== *
     * Init Functions
     * ======================================================== */

    init();

});