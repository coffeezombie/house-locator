app.controller('CanvasController', function ($rootScope, $scope, $state, CANVAS, House, HouseService)
{
	function init()
	{
        $rootScope.canvas = new Canvas('site-canvas', {
            canvasConfig: CANVAS,
            canvasContainer: $('#canvas-container'),
            hoverCursor: 'pointer',
            selection: false,
            houseSelectCallback: $scope.canvasHouseSelected,
            houseDeselectCallback: $scope.canvasHouseDeselected
        });

        //$scope.houses.$promise.then(function(houses) {
        //    $rootScope.canvasHouses = HouseService.addHousesToCanvas(houses, $rootScope.canvas, false);
        //});
	}


	/* ======================================================== *
	 * Public Functions
	 * ======================================================== */

    $scope.canvasHouseSelected = function(canvasHouse){
    	$state.go('site.employee', {id: canvasHouse.id});
    }

    $scope.canvasHouseDeselected = function(canvasHouse){
    	$state.go('site.search');
    }


	/* ======================================================== *
	 * Init
	 * ======================================================== */

    init();

});