app.controller('MainController', function ($rootScope, $scope, CONFIG, CANVAS, SocketService, House, HouseService, Employee)
{
    function init()
    {
        getData();
        listenForUpdates();
    }


    /* ======================================================== *
     * Private Functions
     * ======================================================== */


    function getData()
    {
        $rootScope.houses = House.query(function(houses) {
            //HouseService.updateCanvasHousesFromHouses($scope.canvasHouses, houses);

            $scope.canvas.clear();
            $rootScope.canvasHouses = HouseService.addHousesToCanvas(houses, $rootScope.canvas, false);

            $scope.canvas.initYouAreHereIndicator(0.36, 0.9, false);

            $scope.canvas.renderAll();
        });
        $rootScope.employees = Employee.query(function(employees) {
            for(var i in employees) {
                var employee = employees[i];

                employee.photo = $scope.getEmployeePhoto(employee);
            }
        });
    }

    function listenForUpdates()
    {
        SocketService.socket.on('update', function() {
            getData();
        });
    }


    /* ======================================================== *
     * Init Functions
     * ======================================================== */

    init();

});