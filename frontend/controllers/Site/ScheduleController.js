app.controller('ScheduleController', function ($rootScope, $scope, $http, Restangular) {
    function init()
    {
        console.log(Restangular);
        $scope.test = "Test"

        $scope.$on('$viewContentLoaded', pageLoad);
    }

    function pageLoad()
    {
        // Reset canvas selection
        $scope.canvas.deactivateAll().renderAll();

        populateCalendar();
    }


    /* ======================================================== *
     * Public Functions
     * ======================================================== */




    /* ======================================================== *
     * Private Functions
     * ======================================================== */

    function populateCalendar() {
        console.log("Getting calendar events...");
    }


    /* ======================================================== *
     * Init Functions
     * ======================================================== */

    init();

});