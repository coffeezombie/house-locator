app.controller('EmployeeController', function ($rootScope, $scope, $stateParams, $state, $http, CONFIG, messageCenterService, HouseService, Employee)
{
    function init()
    {
        // Get employee id
        var id = $stateParams.id;

        // Get employee data
        $scope.employees.$promise.then(function() {
            $scope.employee = $scope.employees.filter(function(employee) {
                return employee.id == id;
            })[0];

            if($scope.employee === null || typeof $scope.employee === 'undefined') {
                $state.go('site.search');
                return;
            }

            HouseService.selectCanvasHouseFromHouseId($scope.canvas, $scope.employee.House.id);
        });
    }


    /* ======================================================== *
     * Public Functions
     * ======================================================== */

    $scope.sendSMS = function() {
        $http.post(CONFIG.api + '/sms', {
            phone: $scope.employee.phone,
            messageType: 'inLobby'
        }).success(function(response) {
            console.log("Text sent:", response);
            messageCenterService.add('success', $scope.employee.firstName + " " + $scope.employee.lastName + " has been notified.", { timeout: 3000 });
        }).error(function(error, code) {
            console.log(error, code);
            if(code == 429) {
                messageCenterService.add('info', "Please wait before sending another notification.", { timeout: 3000 });
            } else {
                messageCenterService.add('danger', "There was a problem sending the notification.", { timeout: 3000 });
            }
        });
    }


    /* Still needs to be called at some point after the response from the 
     * employee is sent to the twilio application. Not quite sure when 
     * the best moment is.
     */
    $scope.retrieveResponse = function() {
    	$http.get(CONFIG.api + '/smsResponse', {
    	}).success(function(response) {
    		$scope.responseFrom = response.from;
    		$scope.textBody = response.body;
    		console.log("Retreiving incoming message.");
    		messageCenterService.add('success', "Most recent response:" + $scope.textBody);
    	}).error(function(error, code) {
            console.log(error, code);
            messageCenterService.add('danger', "There was a problem retrieving the notification.", { timeout: 3000 });
        });
    }

    /* ======================================================== *
     * Init Functions
     * ======================================================== */

    init();

 });