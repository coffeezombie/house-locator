app.controller('LoginController', function($rootScope, $scope, $state, AUTH_EVENTS, AuthService)
{
    function init() {
        if( AuthService.redirectOnAuthorized() ) {
            return;
        }

        $scope.credentials = {
            username: '',
            password: ''
        };
    }


    /* ======================================================== *
     * Public Functions
     * ======================================================== */

    $scope.login = function(credentials) {
        AuthService.login(credentials).then(function(data) {
            $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
            $scope.setCurrentUser(data.user);
            $state.go('admin.employees');
        }, function (response) {
            console.log("Error:", response.data.message);
            $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
        });
    };


    /* ======================================================== *
     * Init
     * ======================================================== */

    init();

});