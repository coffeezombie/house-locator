app.factory('House', function($resource, CONFIG)
{
    var resource = CONFIG.api + '/houses/:id';

    return $resource(resource, { id: '@id' }, {
        update: {
            method: 'PUT'
        }
    });
});