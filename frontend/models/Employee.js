app.factory('Employee', function($resource, CONFIG)
{
    var resource = CONFIG.api + '/employees/:id';

    return $resource(resource, { id: '@id' }, {
        update: {
            method: 'PUT'
        }
    });
});