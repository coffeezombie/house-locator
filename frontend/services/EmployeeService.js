app.service('EmployeeService', function(CONFIG, MultipartUpload, Employee)
{
    var service = this;
    var resource = CONFIG.api + '/employees';

    this.update = function(employee, successCallback, errorCallback) {
        MultipartUpload.upload(employee, resource + '/' + employee.id, 'PUT', successCallback, errorCallback);
    }

    //this.setEmployeeHouses = function(employees, houses)
    //{
    //    for(var i in houses) {
    //        var house = houses[i];
    //
    //        var employee = _.find(employees, function(e) {
    //            return e.id = house.EmployeeId;
    //        });
    //
    //        // These are the only properties set from canvas
    //        employee.House.x = house.x;
    //        employee.House.y = house.y;
    //        employee.House.angle = house.angle;
    //
    //    }
    //}

});