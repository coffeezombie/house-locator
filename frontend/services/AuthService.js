app.service('AuthService', function ($http, $state, CONFIG, Session)
{
    this.login = function(credentials) {
        var url = CONFIG.api + '/auth/login';
        return $http.post(url, credentials).then(function (res) {
            Session.create(res.data.sessionId, res.data.token, res.data.user);
            return res.data;
        });
    };

    this.logout = function() {
        var url = CONFIG.api + '/auth/logout';

        if( !Session.id ) {
            return false;
        }

        return $http.post(url, {sessionId: Session.id}).success(function(data) {
            Session.destroy();
        }).error(function(error) {
            console.log("Error logging out:", error);
            Session.destroy();
        });
    };

    this.isAuthenticated = function() {
        return !!Session.user;
    };

    this.redirectOnUnauthorized = function(state)
    {
        state = (typeof state !== 'undefined') ? state : 'login';

        if( !this.isAuthenticated() ) {
            $state.go(state);
            return true;
        }

        return false;
    };

    this.redirectOnAuthorized = function(path)
    {
        state = (typeof state !== 'undefined') ? state : 'admin';

        if( this.isAuthenticated() ) {
            $state.go(state);
            return true;
        }

        return false;
    };
});