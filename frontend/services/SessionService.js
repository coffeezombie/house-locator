app.service('Session', function ()
{
    var lsSessionId = 'hlSessionId';
    var lsSessionToken = 'hlSessionToken';
    var lsSessionUser = 'hlSessionName';

    this.id = localStorage[lsSessionId] ? localStorage[lsSessionId] : null;
    this.token = localStorage[lsSessionToken] ? localStorage[lsSessionToken] : null;
    this.user = localStorage[lsSessionUser] ? JSON.parse(localStorage[lsSessionUser]) : null;

    this.create = function(id, token, user) {
        this.destroy();

        this.id = id;
        this.token = token;
        this.user = user;

        localStorage[lsSessionId] = this.id;
        localStorage[lsSessionToken] = this.token;
        localStorage[lsSessionUser] = JSON.stringify(this.user);
    };

    this.destroy = function() {
        this.id = null;
        this.token = null;
        this.user = null;

        localStorage.removeItem(lsSessionId);
        localStorage.removeItem(lsSessionToken);
        localStorage.removeItem(lsSessionUser);
    };

    return this;
});