app.service('HouseService', function(House)
{
    var service = this;

    this.selectCanvasHouseFromHouseId = function(canvas, houseId)
    {
        var canvasHouse = _.find(canvas.getObjects(), function(object) {
            return object.id == houseId;
        });

        canvas.setActiveObject(canvasHouse);
    }

    this.bounceCanvasHouse = function(canvas, houseId)
    {
        var canvasHouse = _.find(canvas.getObjects(), function(object) {
            return object.id == houseId;
        });

        canvasHouse.bounce(canvas);
    }

    this.updateHousesFromCanvasHouses = function(houses, canvasHouses)
    {
        console.log(houses, canvasHouses);

        for(var i in canvasHouses) {
            var canvasHouse = canvasHouses[i];
            var house = _.find(houses, function(h) {
                return h.id === canvasHouse.id;
            });

            house = this.setHouseFromCanvasHouse(house, canvasHouse);
        }

        return houses;
    }

    this.updateCanvasHouseFromHouse = function(canvasHouse, house)
    {
        canvasHouse.percentX = house.x;
        canvasHouse.percentY = house.y;
        canvasHouse.angle = house.angle;
        canvasHouse.label = house.number;

        // Sets x and y from percentages
        canvasHouse.updateScale();

        // Correctly updates selectable region
        canvasHouse.setCoords();

        return canvasHouse;
    }

    this.updateCanvasHousesFromHouses = function(canvasHouses, houses)
    {
        for(var i in canvasHouses) {
            var canvasHouse = canvasHouses[i];
            var house = _.find(houses, function(h) {
                return h.id === canvasHouse.id;
            });

            service.updateCanvasHouseFromHouse(canvasHouse, house);
        }

        return canvasHouses;
    }

    this.setHouseFromCanvasHouse = function(house, canvasHouse)
    {
        house.x = canvasHouse.getPositionPercentages().x;
        house.y = canvasHouse.getPositionPercentages().y;
        house.angle = canvasHouse.angle;
        //house.width = canvasHouse.width;
        //house.height = canvasHouse.height;
        //console.log( canvasHouse );

        return house;
    }

    this.addHousesToCanvas = function(houses, canvas, editable)
    {
        if(!Array.isArray(houses)) {
            houses = [houses];
        }

        var canvasHouses = [];
        for(var i in houses) {
            var data = houses[i];

            if (data.hasOwnProperty('id')) {
                var house = null;

                var options = {
                    id: data.id,
                    canvas: canvas,
                    employeeId: data.EmployeeId || data.employeeId,
                    imageElement: 'house-image',
                    percentX: data.x,
                    percentY: data.y,
                    angle: data.angle,
                    label: data.number
                };

                if(editable) {
                    house = new CanvasBuilding(options);
                } else {
                    house = new CanvasStaticBuilding(options);
                }

                canvasHouses.push(house);
                canvas.add(house);
            }
        }

        return canvasHouses;
    }
});
