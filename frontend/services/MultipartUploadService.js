app.service('MultipartUpload', function ($http, Session) {

    this.upload = function(data, url, method, successCallback, errorCallback){
        var fd = new FormData();
        for(var key in data) {
            fd.append(key, data[key]);
        }

        if( !!Session.userId ) {
            fd.append('sessionToken', Session.token);
            fd.append('sessionId', Session.id);
            fd.append('sessionUserId', Session.userId);
        }

        var req = {
            method: method,
            url: url,
            headers: {'Content-Type': undefined},
            transformRequest: angular.identity,
            data: fd
        };

        $http(req).success(successCallback).error(errorCallback);
    }

});