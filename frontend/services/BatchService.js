app.service('BatchService', function(Util)
{
    var service = this;

    this.batchSave = function(Resource, entities, callback)
    {
        var entityCopies = [];
        for(var i in entities) {
            entityCopies.push( $.extend(true, {}, entities[i]) );
        }

        var successes = [];
        var errors = [];

        var _callback = function()
        {
            if(successes.length + errors.length == entityCopies.length) {
                callback(successes, (errors.length > 0 ? errors : null));
            }
        }

        if(entityCopies.length == 0) {
            _callback();
        }

        for(var i in entityCopies) {
            var entity = entityCopies[i];

            if(!entity.hasOwnProperty('id')) {
                continue;
            }

            delete entity.id;
            delete entity['id'];

            Resource.save(entity, function(successData) {
                successes.push(successData);
                _callback();
            }, function(errorData) {
                errors.push(errorData);
                _callback();
            });
        }
    }

    this.batchUpdate = function(Resource, entities, callback)
    {
        var successes = [];
        var errors = [];

        var _callback = function()
        {
            if(successes.length + errors.length == entities.length) {
                callback(successes, (errors.length > 0 ? errors : null));
            }
        }

        if(entities.length == 0) {
            _callback();
        }

        for(var i in entities) {
            var entity = entities[i];

            if(!entity.hasOwnProperty('id')) {
                continue;
            }

            Resource.update(entity, function(successData) {
                successes.push(successData);
                _callback();
            }, function(errorData) {
                errors.push(errorData);
                _callback();
            });
        }
    }
});