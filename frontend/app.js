var app = angular.module('houseLocatorApp', [
    'ui.router',
    'ngResource',
    'ngAnimate',
    'restangular',
    'MessageCenterModule'
]);